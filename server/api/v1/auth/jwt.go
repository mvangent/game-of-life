package auth

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/mvangent/game-of-life/server/helper"
)

var users = map[string]string{
	"test": "password",
}

var JwtKey = []byte("AAAABBBBCCCCDDDD")

type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

// Create the Signin handler
func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if r.Method == http.MethodOptions {
		return
	}

	log.Printf("Authentication attempt")
	var creds Credentials
	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		helper.RespondWithError(
			w,
			http.StatusBadRequest,
			"Not a valid JSON body?")
		return
	}
	if len(creds.Username) < 1 || len(creds.Password) < 1 {
		helper.RespondWithError(
			w,
			http.StatusBadRequest,
			"Request body needs <username> & <password>")
		return
	}

	expectedPassword, ok := users[creds.Username]
	if !ok || expectedPassword != creds.Password {
		helper.RespondWithError(
			w,
			http.StatusUnauthorized,
			"wrong user/password")
		return
	}
	log.Printf("Found '%s' among valid usernames\n", creds.Username)

	expirationTime := time.Now().Add(5 * time.Minute)
	claims := &Claims{
		Username: creds.Username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	log.Printf("Issueing JWT for '%s'", creds.Username)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(JwtKey)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	data := map[string]interface{}{
		"claims":  claims,
		"jwt":     tokenString,
		"message": "Login successful",
	}
	helper.RespondWithJSON(w, http.StatusOK, data)
}
