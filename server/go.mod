module gitlab.com/mvangent/game-of-life/server

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.3.0
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/objx v0.2.0 // indirect
)
