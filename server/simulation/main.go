package simulation

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"

	"gitlab.com/mvangent/game-of-life/server/grids"
)

type Simulation struct {
	Grid *grids.Grid
}

func (s *Simulation) Marshal() []byte {
	jsonState, err := json.Marshal(&s.Grid)
	if err != nil {
		log.Fatalf("%s\n", err)
	}
	return jsonState
}

func (s *Simulation) Init(cols, rows int) {
	s.Grid = grids.New(cols, rows)
	for y := 0; y < s.Grid.Rows; y++ {
		for x := 0; x < s.Grid.Cols; x++ {
			// init fields with dead state
			s.Grid.Set(x, y, false)
		}
	}
}

func (s *Simulation) RandomChange() {
	// simulate a change for a random point and toggle its value
	x := rand.Intn(s.Grid.Cols)
	y := rand.Intn(s.Grid.Rows)
	val := s.Grid.Get(x, y)
	s.Grid.Set(x, y, !val.(bool))
}

type Cell struct {
	X int
	Y int
}

func (c *Cell) Neighbours(g *grids.Grid) int {
	// the deciding criterion for a cell's life
	var neighbours int = 0
	//fmt.Printf("asking: %+v\n", g.Values)
	for y := c.Y - 1; y <= c.Y+1; y++ {
		for x := c.X - 1; x <= c.X+1; x++ {
			val := g.Get(x, y)
			if val == true {
				//fmt.Printf("%d,%d: %t\n", x, y, val)
				neighbours++
			}
		}

	}

	if g.Get(c.X, c.Y) == true {
		return neighbours - 1
	}

	return neighbours
}

func (c *Cell) Alive(g *grids.Grid) bool {
	// we cast this to a bool for reliabilities sake
	// will panic when there is a wrong value
	return g.Get(c.X, c.Y).(bool)
}

func (s *Simulation) NextTick() {
	var aliveCount int
	var deadCount int

	var new_grid = grids.New(s.Grid.Cols, s.Grid.Rows)

	for y := 0; y < s.Grid.Rows; y++ {
		for x := 0; x < s.Grid.Cols; x++ {
			cell := Cell{X: x, Y: y}
			neighbours := cell.Neighbours(s.Grid)
			alive := cell.Alive(s.Grid)
			if !alive && neighbours == 3 {
				// if cell is dead and has 3 alive neighbours return true
				//log.Printf("alive %d,%d: neighbours: %d: dead and n=3\n", x, y, neighbours)
				new_grid.Set(x, y, true)
				aliveCount++
				//fmt.Printf("%d, %d: %t => true\n", x, y, alive)
			} else if alive && 1 < neighbours && neighbours < 4 {
				// if cell is alive and has > 1 && < 4 living neighbours return true
				//log.Printf("alive %d,%d: neighbours: %d: 1<n<4\n", x, y, neighbours)
				new_grid.Set(x, y, true)
				aliveCount++
				//fmt.Printf("%d, %d: %t => true\n", x, y, alive)
			} else {
				// else return false
				new_grid.Set(x, y, false)
				//log.Printf("dieing %d,%d: neighbours: %d\n", x, y, neighbours)
				//fmt.Printf("%d, %d: %t => false\n", x, y, alive)
				deadCount++
			}
		}
	}

	s.Grid = new_grid
	log.Printf("Simulated a Tick: alive:%d dead: %d\n", aliveCount, deadCount)
}

func FormatGame(s *Simulation) {
	for y := 0; y < s.Grid.Rows; y++ {
		for x := 0; x < s.Grid.Cols; x++ {
			fmt.Printf("%t ", s.Grid.Get(x, y))
		}
		fmt.Println("")
	}

}
