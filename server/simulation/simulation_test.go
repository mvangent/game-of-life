package simulation

import (
	"testing"
)

func TestInitSimulation(t *testing.T) {
	s := Simulation{}
	s.Init(10, 10)
	x := s.Grid.Cols
	y := s.Grid.Rows
	if x != 10 {
		t.Errorf("s.Grid.Cols was incorrect, got: %d, want: %d.", s.Grid.Cols, 10)
	}
	if y != 10 {
		t.Errorf("s.Grid.Rows was incorrect, got: %d, want: %d.", s.Grid.Rows, 10)
	}
	for y := 0; y < s.Grid.Rows; y++ {
		for x := 0; x < s.Grid.Cols; x++ {
			if s.Grid.Get(x, y).(bool) != false {
				t.Errorf("s.Grid.Get(%d, %d) was incorrect, got: %t, want %t.",
					x, y, s.Grid.Get(x, y), false)
			}
		}
	}
}

func TestCellNeighboursCorner(t *testing.T) {
	s := Simulation{}
	s.Init(10, 10)
	s.Grid.Set(1, 0, true)
	c := Cell{X: 1, Y: 0}
	n0 := c.Neighbours(s.Grid)
	if n0 != 0 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n0, 0)
	}

	s.Grid.Set(2, 0, true)
	n1 := c.Neighbours(s.Grid)
	if n1 != 1 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n1, 1)
	}

	s.Grid.Set(1, 1, true)
	n2 := c.Neighbours(s.Grid)
	if n2 != 2 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n2, 2)
	}

	s.Grid.Set(2, 1, true)
	n3 := c.Neighbours(s.Grid)
	if n3 != 3 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n3, 3)
	}

}

func TestCellNeighbours(t *testing.T) {
	s := Simulation{}
	s.Init(10, 10)
	s.Grid.Set(4, 4, true)
	c := Cell{X: 4, Y: 4}
	n0 := c.Neighbours(s.Grid)
	if n0 != 0 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n0, 0)
	}

	s.Grid.Set(5, 4, true)
	n1 := c.Neighbours(s.Grid)
	if n1 != 1 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n1, 1)
	}

	s.Grid.Set(3, 4, true)
	n2 := c.Neighbours(s.Grid)
	if n2 != 2 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n2, 2)
	}

	s.Grid.Set(4, 3, true)
	n3 := c.Neighbours(s.Grid)
	if n3 != 3 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n3, 3)
	}
	s.Grid.Set(4, 5, true)
	n4 := c.Neighbours(s.Grid)
	if n4 != 4 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n4, 4)
	}

	s.Grid.Set(3, 3, true)
	n5 := c.Neighbours(s.Grid)
	if n5 != 5 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n5, 5)
	}

	s.Grid.Set(5, 5, true)
	n6 := c.Neighbours(s.Grid)
	if n6 == 5 {
		t.Errorf("c.Neighbours was incorrect, got: %d, want: %d.", n6, 6)
	}
}

func TestEmptyLeftTopCornerToBecomeAlive(t *testing.T) {
	s := Simulation{}
	s.Init(10, 10)
	s.Grid.Set(0, 1, true)
	s.Grid.Set(1, 0, true)
	s.Grid.Set(1, 1, true)

	s.NextTick()

	p := s.Grid.Get(0, 0)

	if p != true {
		t.Error("point 0 0  SHOULD BE ALIVE, but its not :( .. ) ")
	}

}

func TestEmptyWithThreeNeighboursToBecomeAlive(t *testing.T) {
	s := Simulation{}
	s.Init(6, 6)
	s.Grid.Set(0, 2, true)
	s.Grid.Set(2, 0, true)
	s.Grid.Set(2, 2, true)

	// FormatGame(&s)
	s.NextTick()

	// FormatGame(&s)
	p := s.Grid.Get(1, 1)

	if p != true {
		t.Error("point 1 1 SHOULD BE ALIVE, but its not :( .. ) ")
	}
	//c := Cell{X: 4, Y: 4}
}
