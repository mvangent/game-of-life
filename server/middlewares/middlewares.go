package middlewares

import (
	"log"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	auth "gitlab.com/mvangent/game-of-life/server/api/v1/auth"
)

type AuthenticationMiddleware struct {
	tokenUsers map[string]string
}

//func (amw *AuthenticationMiddleware) Populate() {
//	amw.tokenUsers["00000000"] = "user0"
//	amw.tokenUsers["aaaaaaaa"] = "userA"
//	amw.tokenUsers["05f717e5"] = "randomUser"
//	amw.tokenUsers["deadbeef"] = "user0"
//}

func (amw *AuthenticationMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("X-JWT-Session-Token")
		log.Printf("We got authentication going")
		claims := &auth.Claims{}
		tkn, err := jwt.ParseWithClaims(
			token,
			claims,
			func(token *jwt.Token) (interface{}, error) { return auth.JwtKey, nil })
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				http.Error(w, "Invalid JWT Signature", http.StatusUnauthorized)
				return
			}
			http.Error(w, "Malformed JWT could not be parsed", http.StatusBadRequest)
			return
		}
		if !tkn.Valid {
			http.Error(w, "Invalid/Expired JWT", http.StatusBadRequest)
			return
		}
		log.Printf("Claims %+v are valid", claims)
		next.ServeHTTP(w, r)

		//	if user, found := amw.tokenUsers[token]; found {
		//		next.ServeHTTP(w, r)
		//	} else {
		//		http.Error(w, "Forbidden", http.StatusForbidden)
		//	}
	})
}
