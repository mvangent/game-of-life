package main

import (
	"database/sql"
)

type User struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

func (p *User) getUser(db *sql.DB) error {
	return db.QueryRow("SELECT name, password FROM users WHERE id=$1",
		p.ID).Scan(&p.Name, &p.Password)
}

func (p *User) updateUser(db *sql.DB) error {
	_, err :=
		db.Exec("UPDATE users SET name=$1, password=$2 WHERE id=$3",
			p.Name, p.Password, p.ID)

	return err
}

func (p *User) deleteUser(db *sql.DB) error {
	_, err := db.Exec("DELETE FROM users WHERE id=$1", p.ID)

	return err
}

func (p *User) createUser(db *sql.DB) error {
	err := db.QueryRow(
		"INSERT INTO users(name, password) VALUES($1, $2) RETURNING id",
		p.Name, p.Password).Scan(&p.ID)

	if err != nil {
		return err
	}

	return nil
}

func getUsers(db *sql.DB, start, count int) ([]User, error) {
	rows, err := db.Query(
		"SELECT id, name,  password FROM users LIMIT $1 OFFSET $2",
		count, start)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	Users := []User{}

	for rows.Next() {
		var p User
		if err := rows.Scan(&p.ID, &p.Name, &p.Password); err != nil {
			return nil, err
		}
		Users = append(Users, p)
	}

	return Users, nil
}
