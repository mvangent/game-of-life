package main

import (
	"flag"
	"os"
)

const (
	DefaultServerAddr = "127.0.0.1:9119"
)

type Config struct {
	serverAddr string
	dbHost     string
	dbPort     string
	dbUsername string
	dbPassword string
	dbName     string
}

func parseArgs() *Config {
	var serverAddr string
	flag.StringVar(&serverAddr, "listen",
		DefaultServerAddr, "bind to this interface spec")

	flag.Parse()
	return &Config{
		serverAddr: serverAddr,
		dbHost:     os.Getenv("GOL_DB_HOST"),
		dbPort:     os.Getenv("GOL_DB_PORT"),
		dbUsername: os.Getenv("GOL_DB_USERNAME"),
		dbPassword: os.Getenv("GOL_DB_PASSWORD"),
		dbName:     os.Getenv("GOL_DB_NAME"),
	}
}

func main() {
	app := App{}
	cfg := parseArgs()
	app.Initialize(
		cfg.dbHost,
		cfg.dbPort,
		cfg.dbUsername,
		cfg.dbPassword,
		cfg.dbName,
	)
	app.Run(cfg.serverAddr)
}
