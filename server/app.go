package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	_ "github.com/lib/pq"
	"gitlab.com/mvangent/game-of-life/server/api/v1/auth"
	"gitlab.com/mvangent/game-of-life/server/simulation"
)

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

type App struct {
	Router              *mux.Router
	AuthenticatedRouter *mux.Router
	DB                  *sql.DB
	jwtKey              []byte
	Simulation          *simulation.Simulation
}

type GameConfig struct {
	Rows, Cols int
	Values     []interface{}
}

var Simulation *simulation.Simulation

func (a *App) Initialize(
	host, port,
	user, password,
	dbname string,
) {
	dbConn := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname,
	)
	log.Println(dbConn)

	var err error
	a.DB, err = sql.Open("postgres", dbConn)
	if err != nil {
		log.Fatal(err)
	}

	a.Router = mux.NewRouter()
	a.AuthenticatedRouter = mux.NewRouter()
	a.initializeRoutes()

	a.Simulation = &simulation.Simulation{}
	a.Simulation.Init(10, 10)
}

func (a *App) Run(addr string) {
	log.Fatal(
		http.ListenAndServe(addr, a.Router),
	)
}

func (a *App) initializeRoutes() {
	a.Router.Use(mux.CORSMethodMiddleware(a.Router))
	login := a.Router.PathPrefix("/api/v1/auth").Subrouter()
	login.HandleFunc("/jwt", auth.Login).Methods(
		http.MethodPost,
		http.MethodOptions)

	websockets := a.Router.PathPrefix("/api/v1/ws").Subrouter()
	websockets.HandleFunc("/{id:[0-9]+}/data", a.Data).Methods(
		http.MethodGet,
		http.MethodOptions)

	api := a.Router.PathPrefix("/api/v1").Subrouter()
	// amw := middlewares.AuthenticationMiddleware{}
	// api.Use(amw.Middleware)

	api.HandleFunc("/refresh", a.Refresh).Methods(
		http.MethodGet,
		http.MethodOptions)
	api.HandleFunc("/game/{id:[0-9]+}", a.GameSetup).Methods(
		http.MethodGet,
		http.MethodPost,
		http.MethodOptions)
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

var users = map[string]string{
	"test": "password",
}

type ResponseData map[string]interface{}

var jwtKey = []byte("AAAABBBBCCCCDDDD")

type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

func (a *App) Game(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if r.Method == http.MethodOptions {
		return
	}
	data := ResponseData{
		"message": "Hello & Welcome",
		"errors":  nil,
	}
	respondWithJSON(w, http.StatusOK, data)
}

func (a *App) GameSetup(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if r.Method == http.MethodOptions {
		return
	}
	decoder := json.NewDecoder(r.Body)
	var gameConfig GameConfig
	err := decoder.Decode(&gameConfig)
	if err != nil {
		panic(err)
	}

	a.Simulation.Init(gameConfig.Rows, gameConfig.Cols)
	a.Simulation.Grid.Values = gameConfig.Values
	log.Printf("Game setup: %+v", gameConfig)

	data := ResponseData{
		"message": "OK",
		"errors":  nil,
		"config":  gameConfig,
	}
	respondWithJSON(w, http.StatusOK, data)
}

func (a *App) Data(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if r.Method == http.MethodOptions {
		return
	}
	log.Printf("Initial State: %s\n", a.Simulation.Marshal())
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	log.Printf("Websocket connection upgrade: %+v\n", conn)

	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			fmt.Printf("Received error(%s) on %v received type(%d) messages(%s)\n",
				err, conn, messageType, p)
			return
		}
		log.Printf("Received on websocket type(%d) body(%s)\n", messageType, p)

		gameStateJson := a.Simulation.Marshal()
		log.Printf("Game(%d, %d)", a.Simulation.Grid.Cols, a.Simulation.Grid.Rows)
		a.Simulation.NextTick()
		if err := conn.WriteMessage(messageType, gameStateJson); err != nil {
			fmt.Printf("Websocket write failed type(%d) messages(%s) with error(%s)\n",
				messageType, p, err)
			return
		}
	}
}

func isJWTExpiring(claims *auth.Claims) bool {
	if time.Unix(claims.ExpiresAt, 0).Sub(time.Now()) > 30*time.Second {
		log.Printf("Not refreshing JWT for %s since is still fresh", claims.Username)
		return false
	} else {
		return true
	}

}

func createNewJWT(w http.ResponseWriter, claims *auth.Claims) (string, time.Time) {
	expirationTime := time.Now().Add(15 * time.Minute)
	claims.ExpiresAt = expirationTime.Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		respondWithError(
			w,
			http.StatusInternalServerError,
			":pepe-struggling: during jwt creation")
	}
	log.Printf("Refreshing JWT/Set-Cookie for %s since it is due", claims.Username)
	return tokenString, expirationTime

}

func (a *App) Refresh(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if r.Method == http.MethodOptions {
		return
	}
	claims := &auth.Claims{}
	jwt.ParseWithClaims(
		r.Header.Get("X-JWT-Session-Token"),
		claims,
		func(token *jwt.Token) (interface{}, error) { return jwtKey, nil })

	if !isJWTExpiring(claims) {
		respondWithError(
			w,
			http.StatusBadRequest,
			"Do not refresh JWT so greedily")
	}

	// If we reach this code, renew a new token for the current user
	tokenString, expiresAt := createNewJWT(w, claims)
	http.SetCookie(w, &http.Cookie{
		Name:    "authToken",
		Value:   tokenString,
		Expires: expiresAt,
	})
	data := ResponseData{
		"message": "Refreshed JWT",
		"claims":  claims,
		"errors":  nil,
		"jwt":     tokenString,
	}
	respondWithJSON(w, http.StatusOK, data)
}
