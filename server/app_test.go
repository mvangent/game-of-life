package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var a App

func TestMain(m *testing.M) {
	cfg := parseArgs()
	a.Initialize(
		cfg.dbHost,
		cfg.dbPort,
		cfg.dbUsername,
		cfg.dbPassword,
		cfg.dbName,
	)

	code := m.Run()
	os.Exit(code)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func loginAttempt(user, password string) *httptest.ResponseRecorder {
	message := map[string]interface{}{
		"username": user,
		"password": password,
	}
	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}
	req, _ := http.NewRequest("POST", "/api/v1/auth/jwt", bytes.NewBuffer(bytesRepresentation))
	return executeRequest(req)

}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func TestLoginReturns405onGET(t *testing.T) {
	req, _ := http.NewRequest("GET", "/api/v1/auth/jwt", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}

func TestLoginReturns405onPUT(t *testing.T) {
	req, _ := http.NewRequest("PUT", "/api/v1/auth/jwt", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}

func TestLoginReturns405onHEAD(t *testing.T) {
	req, _ := http.NewRequest("HEAD", "/api/v1/auth/jwt", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}
func TestLoginReturns405onDELETE(t *testing.T) {
	req, _ := http.NewRequest("DELETE", "/api/v1/auth/jwt", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}

func TestLoginReturns200onOPTION(t *testing.T) {
	req, _ := http.NewRequest("OPTION", "/api/v1/auth/jwt", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}

func TestLoginReturns200onPOST(t *testing.T) {
	response := loginAttempt("test", "password")
	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestLoginReturns400onBadPOST(t *testing.T) {
	message := map[string]interface{}{
		"user@name": "test",
		"password":  "password",
	}
	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}
	req, _ := http.NewRequest(
		"POST",
		"/api/v1/auth/jwt",
		bytes.NewBuffer(bytesRepresentation),
	)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
}

func TestLoginReturns401onWrongUser(t *testing.T) {
	message := map[string]interface{}{
		"username": "haxor",
		"password": "password",
	}
	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}
	req, _ := http.NewRequest(
		"POST",
		"/api/v1/auth/jwt",
		bytes.NewBuffer(bytesRepresentation),
	)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusUnauthorized, response.Code)
}

func TestLoginReturns401onWrongPass(t *testing.T) {
	message := map[string]interface{}{
		"username": "test",
		"password": "hax0red",
	}
	bytesRepresentation, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}
	req, _ := http.NewRequest(
		"POST",
		"/api/v1/auth/jwt",
		bytes.NewBuffer(bytesRepresentation),
	)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusUnauthorized, response.Code)
}

func TestGameReturns200onPOST(t *testing.T) {
	resp := loginAttempt("test", "password")
	jsonData := resp.Body.Bytes()
	var data map[string]interface{}
	json.Unmarshal(jsonData, &data)

	values := []byte(`{"Cols":2,"Rows":2,"Values":[false, true, true, true]}`)

	req, err := http.NewRequest("POST", "/api/v1/game/1", bytes.NewBuffer(values))
	if err != nil {
		fmt.Printf("\n%+v\n", err)
		panic(err)
	}
	req.Header.Set("X-JWT-Session-Token", data["jwt"].(string))
	fmt.Printf("%+v", req.Header)
	response := executeRequest(req)
	fmt.Printf("%+v", response)
	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestGameReturns405onPUT(t *testing.T) {
	req, _ := http.NewRequest("PUT", "/api/v1/game/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}

func TestGameReturns405onHEAD(t *testing.T) {
	req, _ := http.NewRequest("HEAD", "/api/v1/game/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}
func TestGameReturns405onDELETE(t *testing.T) {
	req, _ := http.NewRequest("DELETE", "/api/v1/game/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}

func TestGameReturns200onOPTION(t *testing.T) {
	req, _ := http.NewRequest("OPTION", "/api/v1/game/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusMethodNotAllowed, response.Code)
}

// func TestGameReturns200onGET(t *testing.T) {
// 	resp := loginAttempt("test", "password")
// 	jsonData := resp.Body.Bytes()
// 	var data map[string]interface{}
// 	json.Unmarshal(jsonData, &data)
// 	req, _ := http.NewRequest("GET", "/api/v1/game/1", nil)
// 	req.Header.Set("X-JWT-Session-Token", data["jwt"].(string))
// 	response := executeRequest(req)
// 	checkResponseCode(t, http.StatusOK, response.Code)
// }

func TestReturns400onUnrouted(t *testing.T) {
	req, _ := http.NewRequest("OPTION", "/api/v99999/kappa", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, response.Code)
}
