package grids

type (
	Grid struct {
		Values     []interface{}
		Cols, Rows int
	}
)

func New(Cols, Rows int) *Grid {
	return &Grid{
		Values: make([]interface{}, Cols*Rows),
		Cols:   Cols,
		Rows:   Rows,
	}
}

//func (g *Grid) Do(f func(p Point, value interface{})) {
//	for x := 0; x < g.Cols; x++ {
//		for y := 0; y < g.Rows; y++ {
//			f(Point{X: x, Y: y}, g.Values[x*g.Cols+y])
//		}
//	}
//}

func (g *Grid) Get(X, Y int) interface{} {
	if X < 0 || Y < 0 || X >= g.Cols || Y >= g.Rows {
		return nil
	}
	v := g.Values[X*g.Cols+Y]
	return v
}

func (g *Grid) Len() int {
	return g.Rows * g.Cols
}

func (g *Grid) Set(X, Y int, v interface{}) {
	g.Values[X*g.Cols+Y] = v
}
