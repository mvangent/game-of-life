package grids

import "testing"

func TestNew(t *testing.T) {
	grid := New(5, 5)
	if grid.Cols != 5 {
		t.Errorf("Cols was incorrect, got: %d, want: %d.", grid.Cols, 5)
	}
	if grid.Rows != 5 {
		t.Errorf("Rows was incorrect, got: %d, want: %d.", grid.Rows, 5)
	}
	if grid.Len() != 25 {
		t.Errorf("Len() was incorrect, got: %d, want: %d.", grid.Len(), 25)
	}
}

func TestSet(t *testing.T) {
	grid := New(2, 2)
	grid.Set(0, 0, true)
	grid.Set(1, 1, true)

	if grid.Values[0] != true {
		t.Errorf("grid.Values[0] was incorrect, got: %t, want: %t.", grid.Values[0], true)
	}
	if grid.Values[3] != true {
		t.Errorf("grid.Values[0] was incorrect, got: %t, want: %t.", grid.Values[3], true)
	}
}

func TestGet(t *testing.T) {
	grid := New(2, 2)
	grid.Set(0, 0, true)

	if grid.Get(0, 0) != true {
		t.Errorf("grid.Get() was incorrect, got: %t, want: %t.", grid.Get(0, 0), true)
	}
}
