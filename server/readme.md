# upcoming-server

This branch will hold the upcoming server version. Nothing garanteed.

- Will try to refactor into less klutz by using gorilla more.

# to build & run the server

```
cd ./server
go get
go build .
./gol-server
```

# communicate with it

create a temp file for the `authToken`

```
TOKEN=$(mktemp)
```

## authenticate

First we hit the `/login` endpoint and retrieve a JWT. Then we can just
curl around like there is no HTTP/401 in tcp:

```
curl \
  -c $TOKEN \
  -v \
  -d '{"username":"test","password":"password"}' \
  localhost:8000/login
```

Example:

```
*   Trying 127.0.0.1:8000...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8000 (#0)
> POST /login HTTP/1.1
> Host: localhost:8000
> User-Agent: curl/7.65.3
> Accept: */*
> Content-Length: 41
> Content-Type: application/x-www-form-urlencoded
>
* upload completely sent off: 41 out of 41 bytes
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
* Added cookie authToken="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHAiOjE1ODcxNjM1MjV9.fTeXQj_4WTRuCa76fXTgI5buRysegP4rWSPdYLoUNac" for domain localhost, path /, expire 1587163525
< Set-Cookie: authToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHAiOjE1ODcxNjM1MjV9.fTeXQj_4WTRuCa76fXTgI5buRysegP4rWSPdYLoUNac; Expires=Fri, 17 Apr 2020 22:45:25 GMT
< Date: Fri, 17 Apr 2020 22:40:25 GMT
< Content-Length: 0
<
* Connection #0 to host localhost left intact

```

## refresh

Each JWT is valid for 5 minutes. Responsible refreshes are mandatory.

```
curl -c $TOKEN -vb $TOKEN http://localhost:8000/refresh
```

Example:

```
*   Trying 127.0.0.1:8000...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8000 (#0)
> GET /refresh HTTP/1.1
> Host: localhost:8000
> User-Agent: curl/7.65.3
> Accept: */*
> Cookie: authToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHAiOjE1ODcxNjkwODZ9.7wDHb8axpbMPDwCZexiTWTnmIq7SyfPB9ZanPDMfFbA
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
* Replaced cookie authToken="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHAiOjE1ODcxNjkzNzF9.-AacS6WtyVlyq4Mbw1_jwO7RSG0I46QaFjjAFSYJo5M" for domain localhost, path /, expire 1587169371
< Set-Cookie: authToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHAiOjE1ODcxNjkzNzF9.-AacS6WtyVlyq4Mbw1_jwO7RSG0I46QaFjjAFSYJo5M; Expires=Sat, 18 Apr 2020 00:22:51 GMT
< Date: Sat, 18 Apr 2020 00:17:51 GMT
< Content-Length: 0
<
* Connection #0 to host localhost left intact
```

> Be aware that HTTP/400 is returned when request is not within 30secs prior to
> expiration.

## access the queens crown jewels

```
curl -c $TOKEN -vb $TOKEN http://localhost:8000/game
```

Example:

```
*   Trying 127.0.0.1:8000...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8000 (#0)
> GET /welcome HTTP/1.1
> Host: localhost:8000
> User-Agent: curl/7.65.3
> Accept: */*
> Cookie: authToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHAiOjE1ODcxNjM4NDR9.82Vnu86Erps44eN6CkYDaBjHka_Rz6ZH2sMkGA-lxLU
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Fri, 17 Apr 2020 22:45:48 GMT
< Content-Length: 13
< Content-Type: text/plain; charset=utf-8
<
* Connection #0 to host localhost left intact
Hello game &{Username:test StandardClaims:{Audience: ExpiresAt:1587166877 Id: IssuedAt:0 Issuer: NotBefore:0 Subject:}}!
```

> That concludes the basic functions of serving authenticated http for no reason.

# websockets

After a successful authentication, one can use the websocket endpoint like an echo server.

> I hope that, since curl only makes a successful connection upgrade, but is
> not able to transmit actual data

```
curl \
  -vi -N \
  -H "Sec-WebSocket-Key: abc" \
  -H "Sec-Websocket-Version: 13" \
  -H "Connection: Upgrade" \
  -H "Upgrade: websocket" \
  -H "Host: localhost" \
  -H "Origin: http://127.0.0.1" \
  -H "X-JWT-Session-Token: $jwt_value" \
  http://127.0.0.1:8000/api/v1/game/1/data
```

Example:

```
*   Trying 127.0.0.1:8000...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8000 (#0)
> GET /game/data HTTP/1.1
> Host: localhost
> User-Agent: curl/7.65.3
> Accept: */*
> Cookie: authToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QiLCJleHAiOjE1ODcyMTA5NTF9.-KGuDePYCd8IeA4RRTZWn20djiosB3mPvjeHASqsi30
> Sec-WebSocket-Key: abc
> Sec-Websocket-Version: 13
> Connection: Upgrade
> Upgrade: websocket
> Origin: http://localhost
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 101 Switching Protocols
HTTP/1.1 101 Switching Protocols
< Upgrade: websocket
Upgrade: websocket
< Connection: Upgrade
Connection: Upgrade
< Sec-WebSocket-Accept: zwKd5BnoWxQ6GxTxxSmBSHGHX9M=
Sec-WebSocket-Accept: zwKd5BnoWxQ6GxTxxSmBSHGHX9M=

<
Warning: Binary output can mess up your terminal. Use "--output -" to tell
Warning: curl to output it to your terminal anyway, or consider "--output
Warning: <FILE>" to save to a file.
* Failed writing body (0 != 2)
* Closing connection 0
```
