import { Machine, assign, State } from 'xstate';
import { serverEndpoint } from './../appConfig';
import { useGameStream } from './../streams/game';
import { gameOfLifeService } from './service';

interface GameOfLifeStateSchema {
    states: {
        loggedOut: State;
        loggedIn: State;
        gameOfLife: State;
    };
}

type AppEvent =
    | { type: 'EXIT_GAME'; jwt: string }
    | { type: 'SUCCESFUL_LOGIN'; jwt: string }
    | {
          type: 'SETUP_COMPLETE';
          jwt: string;
          payload: { Values: boolean[]; Cols: number; Rows: number };
      }
    | { type: 'LOG_OUT'; jwt: string };

interface GameOfLifeContext {
    jwt: string | null;
}

const loggedOut = {
    on: { SUCCESFUL_LOGIN: { target: 'loggedIn', actions: ['persistToken'] } },
};

const loggedIn = {
    on: {
        SETUP_COMPLETE: {
            target: 'gameOfLife',
            actions: ['setupGameForSocket', 'updateGameParams'],
        },
        LOG_OUT: { target: 'loggedOut', actions: ['removeToken'] },
    },
};

const gameOfLife = {
    entry: ['installGameControls', 'connectToGame'],
    on: {
        START_GAME: { actions: ['startGame'] },
        CLEAR_GAME: { actions: ['disconnectGame'] },
        UPDATE_STATE: { actions: ['updateGameParams'] },
        LOG_OUT: {
            target: 'loggedOut',
            actions: ['removeToken', 'disconnectGame'],
        },
        EXIT_GAME: { target: 'loggedIn', actions: ['disconnectGame'] },
    },
};

const states = { loggedOut, loggedIn, gameOfLife };

const context = { jwt: localStorage.getItem('jwt') };

const updateGameParams = assign({
    gameParams: (context, payload) => {
        return payload.grid;
    },
});

const installGameControls = assign({
    gameControls: (context, payload) => {
        // FIXME: add room id to payload and take it from payload
        const { connect, start, disconnect } = useGameStream({
            id: 1,
            interval: 100,
        });

        return { connect, start, disconnect };
    },
});

const actions = {
    persistToken: (context: GameOfLifeContext, event: { jwt?: string }) => {
        if (event.jwt) localStorage.setItem('jwt', event.jwt);
    },
    removeToken: () => {
        localStorage.removeItem('jwt');
    },
    setupGameForSocket: async function (
        context: GameOfLifeContext,
        payload: {
            grid: { Values: boolean[]; Cols: number; Rows: number };
        },
    ) {
        await fetch(`${serverEndpoint}/api/v1/game/1`, {
            method: 'POST',
            mode: 'cors', // FIXME: set valid origin
            body: JSON.stringify(payload.grid),
        })
            .then((res: any) => {
                if (res.status === 200) {
                    return res.json();
                }

                throw Error('Could not load game', res);
            })
            .catch((err) => console.error(err));
    },
    installGameControls,
    updateGameParams,
    connectToGame: (context) => {
        if (!context.gameControls) throw Error('Make sure to install game controls on the context first');

        const game$ = context.gameControls.connect();

        game$.subscribe((resp: { Values: boolean[]; Cols: number; Rows: number }) => {
            gameOfLifeService.send('UPDATE_STATE', { grid: resp });
        });
    },
    startGame: (context) => {
        if (!context.gameControls) throw Error('Make sure to install game controls on the context first');

        context.gameControls.start();
    },
    disconnectGame: (context) => {
        if (!context.gameControls) throw Error('Make sure to install game controls on the context first');

        context.gameControls.disconnect();
    },
};
const config = {
    id: 'game-of-life',
    context,
    initial: 'loggedOut' as const,
    states,
};

export const golMachine = Machine<GameOfLifeContext, GameOfLifeStateSchema, AppEvent>(config, { actions });
