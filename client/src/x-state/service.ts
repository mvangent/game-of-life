import { interpret } from 'xstate';

import { golMachine } from './machine';

const gameOfLifeService = interpret(golMachine).start();

export { gameOfLifeService };
