interface Preset {
	Values: boolean[];
	Rows: number;
	Cols: number;
}

export { Preset };
