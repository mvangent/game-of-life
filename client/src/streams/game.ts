import { takeUntil } from 'rxjs/operators';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { interval, Observable, Subject } from 'rxjs';

function useGameStream(config: { id: number; interval: number }) {
    let game$: WebSocketSubject<any> | undefined;
    let interval$: Observable<number> | undefined;
    let stopGame$: Subject<boolean> | undefined = new Subject<boolean>();

    return {
        connect: () => {
            if (game$) {
                throw Error('UseGameStream::connect -> The game stream is already defined');
            }

            game$ = webSocket({
                url: `ws://127.0.0.1:9119/api/v1/ws/${config.id}/data`,
            });

            interval$ = interval(config.interval);

            return game$;
        },
        start: () => {
            if (!interval$) {
                throw Error(
                    'UseGameStream::start ->  Could not start the game because there is no connection to the interval stream',
                );
            }

            if (!stopGame$)
                throw Error('UseGameStream::start ->  Stop Game Stream needs to be initialised as a subject');

            interval$.pipe(takeUntil(stopGame$)).subscribe(() => {
                if (!game$)
                    throw Error(
                        'UseGameStream::start -> Could not start the game because the game stream in undefined',
                    );

                game$.next({ message: `next gol state request for room ${config.id}` });
            });
        },
        disconnect: () => {
            if (!game$ || !stopGame$) {
                throw Error('UseGameStream::disconnect -> game stream or stop game stream already undefined');
            }

            stopGame$.next(true);
            stopGame$.complete();
            game$.complete();
            game$ = undefined;
            interval$ = undefined;
            stopGame$ = undefined;
        },
    };
}

export { useGameStream };
