import Vue from 'vue';
import VueCompositionApi from '@vue/composition-api';
import { BootstrapVue, IconsPlugin, ToastPlugin } from 'bootstrap-vue';
import Rx from 'rxjs';
import VueRx from 'vue-rx';

Vue.use(VueCompositionApi);
Vue.use(BootstrapVue);
Vue.use(ToastPlugin);
Vue.use(IconsPlugin);
Vue.use(VueRx, Rx);

Vue.config.productionTip = false;

import App from './src/App.vue';
import './src/custom.scss';

new Vue({ render: (createElement) => createElement(App) }).$mount('#app');
