###

Inspired on conway's game of life (https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

just a hack project to play with golang, sockets, graphics and creativity

### Contributors

mvangent@github.com
krysopath@gitlab.com
