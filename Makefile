GOOS := linux
GOARCH := amd64

.PHONY = server/gol-$(GOOS)-$(GOARCH) gotests

dev/client:
	cd client; yarn dev

gotests:
	-cd server; go test ./... -coverprofile coverage.txt

server/gol-$(GOOS)-$(GOARCH): gotests
	cd server; go build -v -o ./build/gol-$(GOOS)-$(GOARCH) -ldflags='-s -w' .

run/server: server/gol-$(GOOS)-$(GOARCH)
	server/build/gol-$(GOOS)-$(GOARCH) -listen 127.0.0.1:9119

dev/run:
	bash -c "cd client; yarn dev" 2>&1\
		| tee client.log&
	$(MAKE) run/server | tee server.log
